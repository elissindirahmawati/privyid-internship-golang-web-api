package handler

import ( 
	"strconv"
	"fmt"
	"net/http"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"pustaka-api/book"
)

// repository diakses di dalam service. service diakses didalam handler

type bookHandler struct {
	bookService book.Service
}

func NewBookHandler(bookService book.Service) *bookHandler {
	return &bookHandler{bookService}
}
// nama functionnya huruf awalnya kapital biar jadi public dan bisa digunain di folder lain
func (h *bookHandler) GetBooks(c *gin.Context) {
	books, err := h.bookService.FindAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}
	var booksResponse []book.BookResponse
	for _, b := range books {
		bookResponse := convertToBookResponse(b)
		booksResponse = append(booksResponse, bookResponse)
	}
	c.JSON(http.StatusOK, gin.H{
		"data": booksResponse,
	})
}

func (h *bookHandler) GetBook(c *gin.Context) {
	idString:= c.Param("id")
	id, _ := strconv.Atoi(idString)
	b, err := h.bookService.FindByID(int(id))
	
	if err != nil {
	c.JSON(http.StatusBadRequest, gin.H{
		"errors": err,
	}) 
	return
	}
	// convert ke bentuk respon yang dimau
	bookResponse := convertToBookResponse(b)
	c.JSON(http.StatusOK, gin.H{
		"data": bookResponse,
	})
}


func (h *bookHandler) CreateBook(c *gin.Context) {
	// title, price
	var bookRequest book.BookRequest

	err := c.ShouldBindJSON(&bookRequest)
	if err != nil {
		errorMessages := []string{} // buat bikin slices. supaya bisa nampilin error lebih dari 1
		for _, e := range err.(validator.ValidationErrors) {
				errorMessage := fmt.Sprintf("Error on field %s, condition: %s", e.Field(), e.ActualTag())
				errorMessages = append(errorMessages, errorMessage)
			}
			c.JSON(http.StatusBadRequest, gin.H {
				"errors": errorMessages})
				return //supaya ga eksekusi yg dibawahnya
	
		
		//log.Fatal(err) // -> ini kalo error exit
	}
	book, err := h.bookService.Create(bookRequest)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H {
			"errors":err,
		})
		return 
	}
	c.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
		//"sub_title": bookInput.SubTitle,
	})
}

func (h *bookHandler) UpdateBook(c *gin.Context) {
	// title, price
	var bookRequest book.BookRequest

	err := c.ShouldBindJSON(&bookRequest)
	if err != nil {
		errorMessages := []string{} // buat bikin slices. supaya bisa nampilin error lebih dari 1
		for _, e := range err.(validator.ValidationErrors) {
				errorMessage := fmt.Sprintf("Error on field %s, condition: %s", e.Field(), e.ActualTag())
				errorMessages = append(errorMessages, errorMessage)
			}
			c.JSON(http.StatusBadRequest, gin.H {
				"errors": errorMessages})
				return //supaya ga eksekusi yg dibawahnya
	
		
		//log.Fatal(err) // -> ini kalo error exit
	}
	idString:= c.Param("id")
	id, _ := strconv.Atoi(idString)

	book, err := h.bookService.Update(id, bookRequest)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H {
			"errors":err,
		})
		return 
	}
	c.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
		//"sub_title": bookInput.SubTitle,
	})
}

func (h *bookHandler) DeleteBook(c *gin.Context) {
	idString:= c.Param("id")
	id, _ := strconv.Atoi(idString)

	b, err := h.bookService.Delete(int(id))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H {
			"errors":err,
		})
		return 
	}
	bookResponse := convertToBookResponse(b)
	c.JSON(http.StatusOK, gin.H{
		"data": bookResponse,
		//"sub_title": bookInput.SubTitle,
	})
}
// fungsi buat convert. dibikin private = huruf awal kecil
func convertToBookResponse(b book.Book) book.BookResponse {
	return book.BookResponse{
		ID: b.ID,
		Title: b.Title,
		Price: b.Price,
		Description: b.Description,
		Rating: b.Rating,
		Discount: b.Discount,
	}
}