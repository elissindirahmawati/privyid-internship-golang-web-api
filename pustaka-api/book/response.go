package book

// dibuat supaya format yang keluar sesuai yg diinginkan. misal createat sm updateat gausah ditamilin
// terus penulisan 2 kata pake pemisah _ dllnya.
type BookResponse struct { //deklarasi data nya pake struct
	ID			int `json:"id"`
	Title 		string `json:"title"`
	Description	string `json:"description"`
	Price		int `json:"price"`
	Rating 		int `json:"rating"`
	Discount	int `json:"discount"`
}