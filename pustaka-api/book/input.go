package book

import (
	"encoding/json"
)

type BookRequest struct { // struct buat nyimpan data yang di POST atau input
	Title string `json:"title" binding:"required"` // penulisan jsonnya "title" dan binding= required berrti harus diisi
	Price json.Number `json:"price" binding:"required,number"` // wajib diisi dan berupa angka
	Description string `json:"description" binding:"required"`
	Rating json.Number `json:"rating" binding:"required,number"`
	Discount json.Number `json:"discount" binding:"required,number"`
	//SubTitle string `json:"sub_title"`// buat ngasih tau kalo yg ditangkap bentuk json dengan penulisan kayak gt
}