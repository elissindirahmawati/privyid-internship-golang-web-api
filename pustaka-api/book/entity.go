package book

import (
	"time"
)

type Book struct { //deklarasi data nya pake struct
	ID			int
	Title 		string
	Description	string
	Price		int
	Rating 		int
	Discount	int
	CreatedAt	time.Time
	UpdatedAt	time.Time
}